<?php

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'Gestion de Pedidos';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Gestión del almacen</h1>

        <p class="lead">Gestión de Comerciales, Clientes y Pedidos</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4 mb-3">
                <h2>Clientes</h2>

            <p><?= Html::a('Ver Clientes',['/cliente/index'],['class' => 'btn btn-primary']) ?></p>    
               
            </div>
            <div class="col-lg-4 mb-3">
                <h2>Comerciales</h2>

                
             <p><?= Html::a('Ver Comerciales',['/comercial/index'],['class' => 'btn btn-primary']) ?></p>   
            </div>
            <div class="col-lg-4">
                <h2>Pedidos</h2>

             <p><?= Html::a('Ver Pedidos',['/pedido/index'],['class' => 'btn btn-primary']) ?></p> 
               
            </div>
        </div>

    </div>
</div>
